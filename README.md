# Trading Bot
This trading bot is part of my project in the course "Intro To Programming" and
will be written in Python on Quantopian. From Version 2 onwards its running.

#### Version 2 of the Trading Bot (17.04.2018)
Trading bot is up and running. My decision rule is to buy Tesla stock if:
open long position if current price is 5 % above 20 day average.
For the period of 04/01/2015 to 07/08/2015 the bot beats the market and has an
average return of over 5 %.


#### Version 1 of the Trading Bot (10.04.2018)
Currently the Trading Bot is not working and has not been improved for two weeks
due to a problem connected to zipline and google finance, which has yet to be 
solved 


# Introduction to the Project and Functions
To understand the code and the basics related to it, I will answer a few key 
questions and make some differentiations. The Quantopian documentation 
is helpful for that: [Quantopian](https://www.quantopian.com/help#api-toplevel).
The trading bot makes use of Quantopia and Zipline.


#### What is the relationship between Quantopia and Zipline?
Quantopia is a pro profit companny, which provides a platform for testing
trading strategies, whereas Zipline is a non-profit trading
algorithm. Quantopia uses Zipline and updates it continously.
Thus Quantopia is mainting Zipline and fixes bugs (at least that is what we 
wish for).


#### Set_slippage function
It captures price changes due to our trades on historical data when back
testing. Our actions could have influenced the stock price which is captured
with this function.


#### Handle_data
It provides the context of our trade.
A minute bar summarizing an assets trading activity for a one-minute period 
and gives you opening price, closing price, high price. 

Most information are in the code!
