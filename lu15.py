#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 17 19:02:37 2018

@author: christianosterhold
"""

'''
First Trading Bot in Quantopian
'''

def initialize(context):
    context.security = symbol=('TSLA')
    
    #Here we tell our algorithm when to run
    schedule_function(func=action_func,
                      date_rule=date_rules.every_day(),
                      time_rule=time_rules.market_open(hours=0,minutes=1)
                      
    #Give it some Logic and call the action function, but before we need to know our stock

def action_func(context, data):
    
    price_history = data.history(assets=context.security, fields='price', bar_count=10, frequency = 'id')
          # history of 10 days for TSLA stock                                 
    
    average_price = price_history.mean()
          # mean of 10 days for TSLA stock  
                      
    current_price = data.current(assets=context.security, fields='price')
    if data.can_trade(context.security):
          # current price
                     
        if current_price > average_price * 1.03:
            order_target_percent(context.security, 1)
        else:
            order_target_percent(context.security, 0)
          # if this is true trade, if not don't trade
     
                 
print('hello world addition')     


